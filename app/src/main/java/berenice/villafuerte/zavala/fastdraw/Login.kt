package berenice.villafuerte.zavala.fastdraw

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_main.*
import javax.xml.validation.TypeInfoProvider

class Login : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

          // MenuLogin
        //Analytics Event

        //Setup
        setup()
    }

    private fun setup(){
        title="Login"

        signUpButton.setOnClickListener(){
            if (editText_nombreUsuario.text.isNotEmpty() && editText_contraseñaUsuario.text.isNotEmpty()){
                FirebaseAuth.getInstance().
                createUserWithEmailAndPassword(editText_nombreUsuario.text.toString(),
                    editText_contraseñaUsuario.text.toString()).addOnCompleteListener{
                    if (it.isSuccessful){
                        showMenuLogin(it.result?.user?.email?:"",ProviderType.BASIC)
                    }else{
                        showAlert()
                    }
                }
            }
        }

        signInButton.setOnClickListener(){
            if (editText_nombreUsuario.text.isNotEmpty() && editText_contraseñaUsuario.text.isNotEmpty()){
                FirebaseAuth.getInstance().
                signInWithEmailAndPassword(editText_nombreUsuario.text.toString(),
                    editText_contraseñaUsuario.text.toString()).addOnCompleteListener{
                    if (it.isSuccessful){
                        showMenuLogin(it.result?.user?.email?:"",ProviderType.BASIC)
                    }else{
                        showAlert()
                    }
                }
            }
        }
    }

    private fun showAlert() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Wrong")
        builder.setMessage("Please Verify your credentials")
        builder.setPositiveButton("OK", null)
        builder.show()
    }

    private fun showAlertUser() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Wrong")
        builder.setMessage("Please Verify your credentials")
        builder.setPositiveButton("OK", null)
        builder.show()
    }

    private fun showMenuLogin(email:String, provider: ProviderType){
        val homeIntent = Intent(this, menulogin::class.java).apply {
            putExtra("email", email)
            putExtra("provider", provider.name)
        }
        startActivity(homeIntent)
    }

}
