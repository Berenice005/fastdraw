package berenice.villafuerte.zavala.fastdraw

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_exit.*

class Exit : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exit)
        btn_salir.setOnClickListener(){
            var intentMenu= Intent(this,MainActivity::class.java)
            startActivity(intentMenu)
        }
    }
}
