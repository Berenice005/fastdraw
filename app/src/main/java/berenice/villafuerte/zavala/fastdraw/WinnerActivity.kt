package berenice.villafuerte.zavala.fastdraw

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_winner.*

class WinnerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_winner)
        buttonSalir.setOnClickListener(){
            var intent= Intent(this,menulogin::class.java)
            startActivity(intent)
        }

    }
}
