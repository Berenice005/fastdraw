package berenice.villafuerte.zavala.fastdraw

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_menulogin.*

enum class ProviderType{
    BASIC
}

class menulogin : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menulogin)

        imageAchieve.setOnClickListener(){
            val intent = Intent(this,Achievement::class.java)
            startActivity(intent)

        }
        imageHelp.setOnClickListener(){
            val intent = Intent(this,Help::class.java)
            startActivity(intent)

        }
        imageExit.setOnClickListener(){
            val intent = Intent(this,Exit::class.java)
            startActivity(intent)

        }

        btn_play.setOnClickListener(){
            val intent = Intent(this,Instrucciones::class.java)
            startActivity(intent)

        }
    }
}
