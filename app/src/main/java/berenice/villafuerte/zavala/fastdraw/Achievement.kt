package berenice.villafuerte.zavala.fastdraw

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import java.util.ArrayList
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_achievement.*
class Achievement : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_achievement)

        val arrayAchievement = ArrayList<user_ranking>()
        arrayAchievement.add(user_ranking("Carlos Ruiz", "Winner", "500"))
        arrayAchievement.add(user_ranking("Stefan Lemus", "Second", "400"))
        arrayAchievement.add(user_ranking("Ricardo Velazquez", "Third", "300"))
        arrayAchievement.add(user_ranking("Fanny Sanchez", "Fourth ", "100"))
        var recycler = Recycler
        recycler.layoutManager = LinearLayoutManager(this,RecyclerView.VERTICAL, false)
        val recyclerAdapter = Adapter_ranking(arrayAchievement)
        recycler.adapter = recyclerAdapter
    }
}
