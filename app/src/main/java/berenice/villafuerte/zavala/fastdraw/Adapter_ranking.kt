package berenice.villafuerte.zavala.fastdraw

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.util.ArrayList

class Adapter_ranking (val array: ArrayList<user_ranking>) :  RecyclerView.Adapter<Adapter_ranking.ViewHolder>(){
    class ViewHolder (itemview: View): RecyclerView.ViewHolder(itemview) {
        val title: TextView = itemView.findViewById(R.id.textTitleAchievement)
        val desc: TextView = itemView.findViewById(R.id.textDescription)
        val xp: TextView = itemView.findViewById(R.id.textXP)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vista = LayoutInflater.from(parent.context).inflate(R.layout.card_ranking,parent,false)
        return ViewHolder(
            vista
        )
    }

    override fun getItemCount(): Int {
        return array.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = array[position].title
        holder.desc.text = array[position].desc
        holder.xp.text = array[position].xp+" XP"

    }
}