package berenice.villafuerte.zavala.fastdraw

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.util.DisplayMetrics
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.activity_drawing.*
import yuku.ambilwarna.AmbilWarnaDialog
import java.util.*

class DrawingActivity : AppCompatActivity() {

    var mDefaulColor = 0
    var contador = 0
    var reloj: CountDownTimer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drawing)

        start.setOnClickListener(){
           start()
        }
        stop.setOnClickListener(){
            reloj?.cancel()
        }
        reset.setOnClickListener(){
            if(reloj != null) {
                reloj?.cancel()
                reloj?.start()
            }
        }

        mDefaulColor = ContextCompat.getColor(this, R.color.colorPrimary)

        btn_colorPicker.setOnClickListener() {
            openColorPicker()
        }

        val metrics = DisplayMetrics()

        windowManager.defaultDisplay.getMetrics(metrics)
        drawingView.init(metrics)

        btn_PencilMore.setOnClickListener() {
            drawingView.pencilMore()
        }

        btn_PencilLess.setOnClickListener() {
            drawingView.pencilLess()
        }

        btn_eraser.setOnClickListener() {
            drawingView.clear()
        }
    }
    fun move1(){
        val intent = Intent(this,WinnerActivity::class.java)
        startActivity(intent)
    }
    fun move2(){
        val intent = Intent(this,LoserActivity::class.java)
        startActivity(intent)
    }
    fun openColorPicker() {
        val colorPicker =
            AmbilWarnaDialog(this, mDefaulColor, object : AmbilWarnaDialog.OnAmbilWarnaListener {
                override fun onCancel(dialog: AmbilWarnaDialog) {}
                override fun onOk(dialog: AmbilWarnaDialog, color: Int) {

                    mDefaulColor = color

                    drawingView.changeColor(mDefaulColor)
                }
            })
        colorPicker.show()
    }
    fun start(){
        contador++;
        reloj = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                tiempo.setText(
                    String.format(
                        Locale.getDefault(),
                        "00:%d",
                        millisUntilFinished / 1000L
                    )
                )
            }

            override fun onFinish() {
                if(contador != 1){
                    move1()
                }else{
                    move2()
                }

            }
        }.start()
    }
}
