package berenice.villafuerte.zavala.fastdraw

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_instrucciones.*

class Instrucciones : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_instrucciones)

        btn_playgame.setOnClickListener() {
            val intent = Intent(this,DrawingActivity::class.java)
            startActivity(intent)
        }
    }
}
