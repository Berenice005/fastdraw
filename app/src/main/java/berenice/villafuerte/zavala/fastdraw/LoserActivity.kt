package berenice.villafuerte.zavala.fastdraw

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_loser.*

class LoserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loser)
        buttonSalir.setOnClickListener(){
            val intent = Intent(this,menulogin::class.java)
            startActivity(intent)
        }
    }
}
